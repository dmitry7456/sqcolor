if(jQuery) (function($) {
		// Public methods
	$.extend($.fn, {
		sqcolors: function(method, data) {
			
			switch(method) {
				
				// Destroy the control
				case 'destroy':
					$(this).each( function() {
						destroy($(this));
					});
					return $(this);
				
				// Hide the color picker
				case 'hide':
					hide();
					return $(this);
				
				// Show the color picker
				case 'show':
					show( $(this).eq(0) );
					return $(this);
				
				// Get/set the hex color value
				case 'value':
					if( data === undefined ) {
						// Getter
						return $(this).val();
					} else {
						// Setter
						$(this).each( function() {
							updateFromInput($(this).val(data));
						});
					}
					return $(this);
				
				// Initializes the control
				default:
					if( method !== 'create' ) data = method;
					$(this).each( function() {
						init($(this), data);
					});
					return $(this);
			}
			
		}
	});
	// Initialize input elements
	function init(input, settings) {
		
		var sqcolors = $('<div class="sqcolors" />');
		
		// Do nothing if already initialized
		if( input.data('sqcolors-initialized') ) return;
		
		// The input
		input
			.addClass('sqcolors-input')
			.data('sqcolors-initialized', false)
			.wrap(sqcolors)
			.after(
				'<div class="sqcolors-panel ui-grid-b">' + 
					// '<div class="sqcolors-grid">' +
					// '</div>' +
					'<div class="ui-block-a sq-red" style="border: 1px solid black;height:50px;"></div>'+
				    '<div class="ui-block-b sq-green" style="border: 1px solid black;height:50px;"></div>'+
				    '<div class="ui-block-c sq-blue" style="border: 1px solid black;height:50px;"></div>'+

					'<div class="ui-block-a sq-rg" style="border: 1px solid black;height:50px;"></div>'+
				    '<div class="ui-block-b sq-rb" style="border: 1px solid black;height:50px;"></div>'+
				    '<div class="ui-block-c sq-gb" style="border: 1px solid black;height:50px;"></div>'+

					'<span class="ui-block-a sq-black" style="border: 1px solid black;height:50px;"></span>'+
				    '<span class="ui-block-b sq-white" style="border: 1px solid black;height:50px;"></span>'+
				    '<span class="ui-block-c sq-custom" style="border: 1px solid black;height:50px;"></span>'+

			    '</div>'
			);
		input.after('<span class="sqcolors-swatch"><span class="sqcolors-swatch-color"></span></span>');

		updateFromInput(input, false);
		input.data('sqcolors-initialized', true);	
	}
	// Returns the input back to its original state
	function destroy(input) {
		
		var sqcolors = input.parent();
		
		// Revert the input element
		input
			.removeData('sqcolors-initialized')
			.removeClass('sqcolors-input');
		
		// Remove the wrap and destroy whatever remains
		sqcolors.before(input).remove();//check
		
	}
	
	// Shows the specified dropdown panel
	function show(input) {
		
		var sqcolors = input.parent()
			panel = sqcolors.find('.sqcolors-panel');
		
		// Do nothing if uninitialized, disabled, inline, or already open
		if( !input.data('sqcolors-initialized') || 
			input.prop('disabled') || 
			sqcolors.hasClass('sqcolors-focus')
		) return;
		
		hide();
		
		sqcolors.addClass('sqcolors-focus');
		
		panel
			.stop(true, true)
			.fadeIn(100, function() {
				//if( settings.show ) settings.show.call(input.get(0));
			});
	}
	
	// Hides all dropdown panels
	function hide() {
		
		$('.sqcolors-focus').each( function() {
			
			var sqcolors = $(this),
				input = sqcolors.find('.sqcolors-input'),
				panel = sqcolors.find('.sqcolors-panel');
			panel.fadeOut(100, function() {
				//if( settings.hide ) settings.hide.call(input.get(0));
				sqcolors.removeClass('sqcolors-focus');
			});			
		});
	}
	function getColor(_parent,_event){
		var offsetX = _parent.offset().left;
		var offsetY = _parent.offset().top;
		var posX = _event.pageX-offsetX;
		var posY = _event.pageY-offsetY;
		var atX = parseInt(posX/sqcolorsGridScale);
		var atY = parseInt(posY/sqcolorsGridScale);
		return colorArray[atY][atX];
	}
	// Moves the selected picker
	function move(target, event, animate) {
		var swatch_ = target.parents('.sqcolors').find('.sqcolors-swatch');
		var color_ = getColor(target,event.originalEvent);
		var input = target.parents('.sqcolors').find('.sqcolors-input');
		input.val(color_);
		doChange(input,color_);
		swatch_.find('SPAN').css('backgroundColor', color_);
	}
	// Sets the color picker values from the input
	function updateFromInput(input, preserveInputValue) {
		
		var hex,
			hsb,
			opacity,
			x, y, r, phi,
			
			// Helpful references
			sqcolors = input.parent(),
			swatch = sqcolors.find('.sqcolors-swatch');
		
		// Determine hex/HSB values
		hex = input.val();
		if( !hex ){
			hex = 0;
		}
		// hsb = hex2hsb(hex);
		
		// Update input value
		if( !preserveInputValue ) input.val(hex);

		// Fire change event, but only if sqcolors is fully initialized
		if( input.data('sqicolors-initialized') ) {
			doChange(input, hex);
		}
		// Update swatch
		swatch.find('SPAN').css('backgroundColor', hex);
	}
		// Runs the change and changeDelay callbacks
	function doChange(input, hex) {
		
		// var settings = input.data('sqcolors-settings'),
		var	lastChange = input.data('sqcolors-lastChange');
		
		// Only run if it actually changed
		if( !lastChange || lastChange.hex !== hex ) {
			
			// Remember last-changed value
			input.data('sqcolors-lastChange', {
				hex: hex
			});
			
			// Fire change event
			// if( settings.change ) {
			// 	if( settings.changeDelay ) {
			// 		// Call after a delay
			// 		clearTimeout(input.data('sqcolors-changeTimeout'));
			// 		input.data('sqcolors-changeTimeout', setTimeout( function() {
			// 			settings.change.call(input.get(0), hex, opacity);
			// 		}, settings.changeDelay));
			// 	} else {
			// 		// Call immediately
			// 		settings.change.call(input.get(0), hex, opacity);
			// 	}
			// }
			input.trigger('change').trigger('input');
		}
	
	}
	// Handle events
	$(document)
		// Hide on clicks outside of the control
		.on('mousedown.sqcolors touchstart.sqcolors', function(event) {
			if( !$(event.target).parents().add(event.target).hasClass('sqcolors') ) {
				hide();
			}
		})
		// Start moving
		.on('mousedown.sqcolors touchstart.sqcolors', '.sqcolors .sqcolors-panel', function(event) {
			var target = $(this);
			// console.log(target);
			// event.preventDefault();
			$(document).data('sqcolors-target', target);
			move(target, event, true);
		})
		// Move pickers
		// .on('mousemove.sqcolors touchmove.sqcolors', function(event) {
		// 	var target = $(document).data('sqcolors-target');
		// 	if( target ) move(target, event);
		// })
		// Stop moving
		.on('mouseup.sqcolors touchend.sqcolors', function() {
			$(this).removeData('sqcolors-target');
		})
		// Show panel when swatch is clicked
		.on('mousedown.sqcolors touchstart.sqcolors', '.sqcolors-swatch', function(event) {
			var input = $(this).parent().find('.sqcolors-input');
			// event.preventDefault();
			show(input);
		})
		// Show on focus
		.on('focus.sqcolors', '.sqcolors-input', function() {
			var input = $(this);
			if( !input.data('sqcolors-initialized') ) return;
			show(input);
		});

	var sqcolorsGridScale=50;// TODO
	var colorArray = [
		['#FF0000','#00FF00','#0000FF'],
		['#FFFF00','#FF00FF','#00FFFF'],
		['#000000','#FFFFFF','#999999']
	];

})(jQuery);